Welcome to Multi-Touch Image Viewer!
=====================

### Disclaimer: This is a work in progress. ###

**Multi-Touch Image Viewer** is a sample application implemented using Keep-In Touch (https://bitbucket.org/Risvil/keepinmove). Simulates a table containing stacked images. Users can move, rotate and scale each image simultaneously and independently. 

----------