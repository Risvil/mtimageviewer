#include "KimSprite.h"
#include "2d\CCDrawingPrimitives.h"
#include <chrono>

using namespace cocos2d;

#if KIMSPRITE_DEBUG
DebugEvent::DebugEvent()
{
	DebugEvent(0, 0, 0);
}

DebugEvent::DebugEvent(int id, float x, float y)
{
	this->pointID = id;
	this->x = x;
	this->y = y;
	this->lastUpdate = std::chrono::steady_clock::now();
}

bool operator==(const DebugEvent& a, const DebugEvent b)
{
	return a.pointID == b.pointID;
}
#endif

KimSprite::KimSprite() {}

KimSprite::~KimSprite() {}

void KimSprite::OnTouchDown(const kim::TouchEvent& e)
{
	if (m_onTouchDown != nullptr)
	{
		m_onTouchDown(e);
	}

#if KIMSPRITE_DEBUG
	// Add to the list of debug points to draw
	DebugEvent p(e.pointID, e.x, e.y);
	m_pointsMutex.lock();
	m_debugPoints.push_back(p);
	m_pointsMutex.unlock();
#endif
}

void KimSprite::OnTouchUp(const kim::TouchEvent& e)
{
	if (m_onTouchUp != nullptr)
	{
		m_onTouchUp(e);
	}

#if KIMSPRITE_DEBUG
	// Remove from the list of debug points to draw
	m_pointsMutex.lock();
	for (auto it = m_debugPoints.begin(); it != m_debugPoints.end(); ++it)
	{
		if (it->pointID == e.pointID)
		{
			m_debugPoints.erase(it);
			break;
		}
	}
	m_pointsMutex.unlock();
#endif
}

void KimSprite::OnTouchMove(const kim::TouchEvent& e)
{
	if (m_onTouchMove != nullptr)
	{
		m_onTouchMove(e);
	}

#if KIMSPRITE_DEBUG
	// Update debug point position
	m_pointsMutex.lock();
	for (auto it = m_debugPoints.begin(); it != m_debugPoints.end(); ++it)
	{
		if (it->pointID == e.pointID)
		{
			it->x = e.x;
			it->y = e.y;
			it->lastUpdate = std::chrono::steady_clock::now();
			break;
		}
	}
	m_pointsMutex.unlock();
#endif
}

void KimSprite::OnGestureCompleted(kim::Gesture* g)
{
	kim::DoubleClickGesture* dbc = dynamic_cast<kim::DoubleClickGesture*>(g);
	if (dbc != nullptr)
	{
		CCLOG("Received DoubleClickGesture at (%f,%f)", dbc->GetX(), dbc->GetY());
	}

	kim::ZoomPinchGesture* zoom = dynamic_cast<kim::ZoomPinchGesture*>(g);
	if (zoom != nullptr)
	{
		CCLOG("Received ZoomPinchGesture - Scale: %f, Delta: %f, Center: (%f,%f)", zoom->GetScale(), zoom->GetDeltaScale(), zoom->GetX(), zoom->GetY());

		auto appliedScale = getScale() + zoom->GetDeltaScale();
		setScale(appliedScale);
	}

	kim::RotateGesture* rot = dynamic_cast<kim::RotateGesture*>(g);
	if (rot != nullptr)
	{
		CCLOG("Received ZoomPinchGesture - Angle: %f, Delta: %f, Center: (%f,%f)", rot->GetAngle(), rot->GetDeltaAngle(), rot->GetX(), rot->GetY());

		auto appliedRotation = getRotation() + rot->GetDeltaAngle();
		setRotation(appliedRotation);
	}

	kim::DragGesture* drag = dynamic_cast<kim::DragGesture*>(g);
	if (drag != nullptr)
	{
		CCLOG("Received DragGesture - Center: (%f,%f)", drag->GetX(), drag->GetY());

		// Denormalize point
		auto point = Vec2(drag->GetX(), drag->GetY());
		auto visible = Director::getInstance()->getVisibleSize();
		point.x *= visible.width;
		point.y *= visible.height;

		setPosition(point);
	}
}

KimSprite* KimSprite::create()
{
	KimSprite* pSprite = new KimSprite();
	
	if (pSprite->init())
	{
		pSprite->autorelease();

		pSprite->UpdateRegion();

		return pSprite;
	}

	CC_SAFE_DELETE(pSprite);
	return NULL;
}

KimSprite* KimSprite::create(const char* fileName)
{
	KimSprite* pSprite = new KimSprite();

	if (pSprite->initWithFile(fileName))
	{
		pSprite->autorelease();

		pSprite->UpdateRegion();

		return pSprite;
	}	

	CC_SAFE_DELETE(pSprite);
	return NULL;
}

void KimSprite::setPosition(const Vec2& pos)
{
	setPosition(pos.x, pos.y);
}

void KimSprite::setPosition(float x, float y)
{
	Sprite::setPosition(x, y);

	UpdateRegion();
}

void KimSprite::setRotation(float rotation)
{
	Sprite::setRotation(rotation);

	UpdateRegion();
}

void KimSprite::setScale(float scale)
{
	Sprite::setScale(scale);

	UpdateRegion();
}

void KimSprite::draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags)
{
#if KIMSPRITE_DEBUG
	ccDrawColor4F(0, 1, 0, 1);
	auto rect = getTextureRect();
	ccDrawRect(rect.origin, rect.size);

	auto visible = Director::getInstance()->getVisibleSize();

	// Keep track of dead points
	std::vector<DebugEvent> deadPoints;
	auto currentTime = std::chrono::steady_clock::now();

	m_pointsMutex.lock();
	for (const auto& point : m_debugPoints)
	{
		// Check for dead point
		auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - point.lastUpdate);
		const auto lifeTime = std::chrono::milliseconds(2000);
		if (elapsedTime > lifeTime)
		{
			deadPoints.push_back(point);
			continue;
		}

		// Denormalize point
		Vec3 tp;
		tp.x = point.x * visible.width;
		tp.y = point.y * visible.height;

		Mat4 inverse = transform;
		inverse.inverse();

		inverse.transformPoint(&tp);

		ccDrawSolidCircle(Vec2(tp.x, tp.y), 5, 360, 16);
	}
	m_pointsMutex.unlock();

	// Remove dead points
	for (const auto& dead : deadPoints)
	{
		auto position = std::find(m_debugPoints.begin(), m_debugPoints.end(), dead);
		m_debugPoints.erase(position);
	}
#else
	Sprite::draw(renderer, transform, flags);
#endif
}

void KimSprite::setOnTouchDownDelegate(std::function<void(const kim::TouchEvent& e)> delegate)
{
	this->m_onTouchDown = delegate;
}

void KimSprite::setOnTouchMoveDelegate(std::function<void(const kim::TouchEvent& e)> delegate)
{
	this->m_onTouchMove = delegate;
}

void KimSprite::setOnTouchUpDelegate(std::function<void(const kim::TouchEvent& e)> delegate)
{
	this->m_onTouchUp = delegate;
}

void KimSprite::UpdateRegion()
{
	// Calculate region position
	auto rect = getTextureRect();

	auto position = getPosition();
	convertToWorldSpace(position);

	Vec3 pos3(position.x, position.y, 0);

	auto scale = getScale();
	auto sizeX = rect.size.width * scale;
	auto sizeY = rect.size.height * scale;

	rect.origin = Size(pos3.x - sizeX / 2, pos3.y + sizeY / 2);
	rect.size = Size(pos3.x + sizeX / 2, pos3.y - sizeY / 2);

	Vec2 topLeft(rect.origin);
	Vec2 bottomRight(rect.size);

	// Normalize
	auto visibleSize = Director::getInstance()->getVisibleSize();

	topLeft.x = topLeft.x / visibleSize.width;
	topLeft.y = topLeft.y / visibleSize.height;
	bottomRight.x = bottomRight.x / visibleSize.width;
	bottomRight.y = bottomRight.y / visibleSize.height;

	// Region implementation constraints: topLeft must be effectively
	// at the top-left. Same with bottomRight.
	auto x1 = std::min(topLeft.x, bottomRight.x);
	auto y1 = std::min(topLeft.y, bottomRight.y);
	auto x2 = std::max(topLeft.x, bottomRight.x);
	auto y2 = std::max(topLeft.y, bottomRight.y);
	SetSize(x1, y1, x2, y2);
}
