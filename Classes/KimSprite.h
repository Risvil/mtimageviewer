#ifndef _KIMSPRITE_HPP_
#define _KIMSPRITE_HPP_

#include "cocos2d.h"
#include "kim.h"
#include <functional>

#define KIMSPRITE_DEBUG 0

#if KIMSPRITE_DEBUG
struct DebugEvent : public kim::TouchEvent
{
	DebugEvent();
	DebugEvent(int id, float x, float y);

	std::chrono::steady_clock::time_point lastUpdate;
};

bool operator==(const DebugEvent& a, const DebugEvent b);
#endif

class KimSprite : public cocos2d::Sprite, public kim::Region
{
public:
	KimSprite();
	virtual ~KimSprite();
	static KimSprite* create();
	static KimSprite* create(const char* fileName);
	
	virtual void setPosition(const cocos2d::Vec2& pos) override;
	virtual void setPosition(float x, float y) override;
	virtual void setRotation(float rotation) override;
	virtual void setScale(float scale) override;

	virtual void OnTouchDown(const kim::TouchEvent& e) override;
	virtual void OnTouchUp(const kim::TouchEvent& e) override;
	virtual void OnTouchMove(const kim::TouchEvent& e) override;
	virtual void OnGestureCompleted(kim::Gesture* g) override;

	virtual void draw(cocos2d::Renderer* renderer, const cocos2d::Mat4& transform, uint32_t flags) override;

	void setOnTouchDownDelegate(std::function<void(const kim::TouchEvent& e)> delegate);
	void setOnTouchMoveDelegate(std::function<void(const kim::TouchEvent& e)> delegate);
	void setOnTouchUpDelegate(std::function<void(const kim::TouchEvent& e)> delegate);

private:
	void UpdateRegion();

private:
	std::function<void(const kim::TouchEvent& e)> m_onTouchDown;
	std::function<void(const kim::TouchEvent& e)> m_onTouchMove;
	std::function<void(const kim::TouchEvent& e)> m_onTouchUp;

#if KIMSPRITE_DEBUG
	std::mutex m_pointsMutex;
	std::vector<DebugEvent> m_debugPoints;
#endif
};

#endif // _KIMSPRITE_HPP_