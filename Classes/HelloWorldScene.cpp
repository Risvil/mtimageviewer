#include "HelloWorldScene.h"
#include "cocostudio\CocoStudio.h"
#include "ui\CocosGUI.h"
#include <chrono>

#include "KimSprite.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = HelloWorld::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width,
		origin.y + closeItem->getContentSize().height));

	closeItem->setScale(2);

	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 2);

	/////////////////////////////
	// 3. add your codes below...

	// add close button usable from kim
	auto closeButton = KimSprite::create("CloseNormal.png");
	closeButton->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width,
		origin.y + closeItem->getContentSize().height));

	closeButton->setOnTouchUpDelegate([](const kim::TouchEvent& e) {
		Director::getInstance()->end();
	});

	closeButton->setScale(2);
	this->addChild(closeButton, 1);

	// add the images
	std::vector<std::string> images = {
		"image1.jpg",
		"image2.jpg",
		"image3.jpg",
		"image4.jpg",
		"image5.jpg",
		"image6.jpg",
	};

	std::vector<KimSprite*> sprites;
	for (auto& image : images)
	{
		auto spriteImage = KimSprite::create(image.c_str());

		auto drag = std::make_shared<kim::DragGesture>();
		auto doubleClick = std::make_shared<kim::DoubleClickGesture>();
		auto rotate = std::make_shared<kim::RotateGesture>();
		auto zoom = std::make_shared<kim::ZoomPinchGesture>();

		spriteImage->AddAllowedGesture(drag);
		spriteImage->AddAllowedGesture(doubleClick);
		spriteImage->AddAllowedGesture(rotate);
		spriteImage->AddAllowedGesture(zoom);

		spriteImage->setOnTouchDownDelegate(
			// bring this sprite to front
			[spriteImage](const kim::TouchEvent& e) {
				static int nextZOrder = 3;

				// do not modify Z-Order if it is already the front-most element
				auto zOrder = spriteImage->getGlobalZOrder();
				if (zOrder < nextZOrder-1)
				{
					spriteImage->setGlobalZOrder(nextZOrder++);
				}
		});

		// stacking a pile of images

		// set current time as random seed
		auto currentTime = std::chrono::steady_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime.time_since_epoch());
		std::srand(duration.count());

		const int MaxPositionDeviance = 10;
		auto noiseX = CCRANDOM_MINUS1_1() * MaxPositionDeviance * 1;
		auto noiseY = CCRANDOM_MINUS1_1() * MaxPositionDeviance * 1;

		auto size = spriteImage->getContentSize();
		auto posX = visibleSize.width / 2 + noiseX;
		auto posY = size.height / 2 + noiseY;

		spriteImage->setPosition(posX, posY);

		const int MaxRotationDeviance = 5;
		auto rotation = CCRANDOM_MINUS1_1() * MaxRotationDeviance;
		spriteImage->setRotation(rotation);

		spriteImage->setScale(0.25f);

		// add the sprite as a child to this layer
		this->addChild(spriteImage, 1);

		sprites.push_back(spriteImage);
	}

	// add background
	auto spriteBackground = KimSprite::create("background.jpg");
	spriteBackground->setScale(1.2f);
	spriteBackground->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(spriteBackground, 0);

	// init KIM
	m_pEngine = kim::IEngine::CreateEngine();
	if (m_pEngine->Init())
	{
		for (auto& sprite : sprites)
		{
			m_pEngine->RegisterRegion(sprite, kim::IEngine::Priority::FOREGROUND);
		}

		m_pEngine->RegisterRegion(closeButton, kim::IEngine::Priority::FOREGROUND);
		m_pEngine->RegisterRegion(spriteBackground, kim::IEngine::Priority::BACKGROUND);
		m_pEngine->Run();
	}

	return true;
}

void HelloWorld::onExit()
{
	Layer::onExit();

	m_pEngine->Shutdown();
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

//void HelloWorld::menuCloseCallback2(Ref* pSender, ui::Widget::TouchEventType eEventType)
//{
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
//	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
//	return;
//#endif
//
//	Director::getInstance()->end();
//
//#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//	exit(0);
//#endif
//}
